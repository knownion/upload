#!/bin/bash

export FLASK_APP=app.py
export FLASK_ENV=development
export MAX_MEGABYTES_UPLOAD="20"

dirpath=$(dirname $(which $0))

cd "$dirpath"/..

flask run
