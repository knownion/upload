from flask import request, Flask
import os, glob, tempfile, shutil
from werkzeug.utils import secure_filename
import requests as req
from zipfile import ZipFile

UPLOAD_DIR = "/tmp"
EXPECTED_FIELD_NAME = "knownion_doc"
ALLOWED_EXTENSIONS = {'zip'}
PARSER_SERVICE_URL = os.getenv("PARSER_SERVICE_URL")
MAX_MEGABYTES_UPLOAD = int(os.getenv("MAX_MEGABYTES_UPLOAD"))

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_DIR
app.config['MAX_CONTENT_LENGTH'] = MAX_MEGABYTES_UPLOAD * 1024 * 1024


def check_request(request):
    req_args = request.form
    
    if request.method != 'PUT':
        raise Exception("Method not allowed, Use PUT")
    
    if not req_args or 'data' not in req_args:
        raise Exception("this function need form argument. "\
            "In addition this form must have a 'data' field")


def sendto_parse_service(data):
    r = req.post(PARSER_SERVICE_URL, data={"data": data})

    if (r.status_code != 200):
        raise Exception(r.text)
    return r.text


def allowed_extension(filename):
    if not ('.' in filename and \
        filename.rsplit('.')[-1].lower() in ALLOWED_EXTENSIONS):
        raise Exception("Extension must be a .zip")


def allowed_fieldname(request):
    if EXPECTED_FIELD_NAME not in request.files:
        raise Exception("No argument {} was send in your form."\
            .format(EXPECTED_FIELD_NAME))
            

def select_markdown_file(temp_dir):
    os.chdir(temp_dir)
    for file in glob.glob("*.md"):
        return open(file, 'r').read()


def extract_markdown(request):
    with tempfile.TemporaryFile() as tmpf:
        file = request.files[EXPECTED_FIELD_NAME]
        file.filename = secure_filename(file.filename)
        tmpf.write(file.read())
        tmpf.seek(0)
        with ZipFile(tmpf, 'r') as zf:
            tdir = str(tempfile.mkdtemp())
            zf.extractall(tdir)
            mdfile = select_markdown_file(tdir)
            shutil.rmtree(tdir)
            return mdfile


def run(request):
    allowed_fieldname(request)
    allowed_extension(request.files[EXPECTED_FIELD_NAME].filename)    
    data = extract_markdown(request)

    jsparsed = sendto_parse_service(data)
    return jsparsed


@app.route('/', methods=['PUT'])
def main():
    result = None
    try:
        result = run(request)
    except Exception as err:
        return "Error: " + str(err), 400

    return result